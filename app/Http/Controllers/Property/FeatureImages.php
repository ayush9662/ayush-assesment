<?php

namespace App\Http\Controllers\Property;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Property;
use App\Featuredimage;

class FeatureImages extends Controller
{
    //
    public function index($id)
    {
        $id=base64_decode($id);
        $data=Property::where('propertyId',$id)->first();
        if($data)
        {
            $images=Featuredimage::where('featurePropertyId',$id)->get();
            return view('images.add-featured-images',compact('data','images'));
        }
        return redirect()->route('property.index')->with('warning',"No record found");
        
    }

    //image upload
    public function imageUpload(Request $request)
    {
        $request->validate([
            'featured' => 'required|image|mimes:jpeg,png,jpg|max:3000|min:1',
        ],[
            "featured.required"=>"Please select one featured image",
            "featured.image"=>"File should be an image",
            "featured.mimes"=>"Files should be jpeg,png,jpg",
            "featured.max"=>"File size should not be greater than 3 MB",
            "featured.min"=>"Please select a valid file"
        ]);
  
         
  
          if ($request->file('featured')) {
              $imagePath = $request->file('featured');
              $imageName = $imagePath->getClientOriginalName();
  
              $path = $request->file('featured')->storeAs('uploads', $imageName, 'public');
          }

          $image = new Featuredimage;
          $image->image = $imageName;
          $image->featurePropertyId = $request->input('propertyId');
          $image->save();
  
          return response()->json('Image uploaded successfully');
    }

    //
    public function delete($id)
    {
        $id=base64_decode($id);
        Featuredimage::where('featuredImageId',$id)->delete();
        return redirect()->back()->with('success','Featured image has been deleted successfully');
    }
}
