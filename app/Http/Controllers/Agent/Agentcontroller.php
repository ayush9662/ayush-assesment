<?php

namespace App\Http\Controllers\Agent;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\Profile;
use App\Message;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Mail;


class Agentcontroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data=Message::where('enquiredUser',Session::get('userid'))->paginate(10);
        return view('agent.dashboard',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $data=$request->all();
        if(isset($data['mail']))
        {
            $request->validate([
                'subject'=>'required',
                'content'=>'required'
            ]);
            $email=$data['email'];
            $maildata=array("content"=>$data['content']);
            $subject=$data['subject'];
           Mail::send('mail',$maildata,function($message) use ($email,$subject)
            {
                $message->to($email)->subject("$subject");
                $message->from("info@dealswid.com","Realstate");
            });
            return redirect()->back()->with('success','Email has been sent successfully');

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $id=base64_decode($id);
        $data=Message::where('messageId',$id)->first();
        return view('agent.view-enquiry',compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data=Profile::getProfileDetails($id);
        return view('agent.profile',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $data=$request->all();
        
        $updated_record=Profile::updateUserData($data,$id);
        if($updated_record)
        {
            return redirect()->back()->with('success','You have updated profile successfully');

        }
        return redirect()->back()->with('warning','Profile update failed');
       

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
