@extends('layout.layout')

@section('title','Enquiry View')

@section('content')
<style>
* {
  box-sizing: border-box;
}

/* Style inputs */
input[type=text], select, textarea {
  width: 100%;
  padding: 12px;
  border: 1px solid #ccc;
  margin-top: 6px;
  margin-bottom: 16px;
  resize: vertical;
}

input[type=submit] {
  background-color: #04AA6D;
  color: white;
  padding: 12px 20px;
  border: none;
  cursor: pointer;
}

input[type=submit]:hover {
  background-color: #45a049;
}

/* Style the container/contact section */
.container {
  border-radius: 5px;
  background-color: #f2f2f2;
  padding: 10px;
}

/* Create two columns that float next to eachother */
.column {
  float: left;
  width: 50%;
  margin-top: 6px;
  padding: 20px;
}

/* Clear floats after the columns */
.row:after {
  content: "";
  display: table;
  clear: both;
}

/* Responsive layout - when the screen is less than 600px wide, make the two columns stack on top of each other instead of next to each other */
@media screen and (max-width: 600px) {
  .column, input[type=submit] {
    width: 100%;
    margin-top: 0;
  }
}

</style>
<div class="container">
  <div style="text-align:center">
    <h2>Enquiry Details</h2>
    
  </div>
  <div class="row">
    <div class="column">
      <table  class="table">
      <tr><td><b>Person Name : </b></td><td>{{$data->name}}</td></tr>
      <tr><td><b>Email : </b></td><td>{{$data->email}}</td></tr>
      <tr><td><b>Contact : </b></td><td>{{$data->contact}}</td></tr>
      <tr><td><b>Message : </b></td><td>{{$data->message}}</td></tr>
      </table>
    </div>
    <div class="column">
    @if (Session::get('success'))
        <div class="alert alert-success alert-dismissible">
            <button style="color:#fff" type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

            {{Session::get('success')}}
        </div>
    @endif
      <form action="{{route('agent.store')}}" method="post" >
      @csrf
        <label for="name">Email</label>
        <input type="text" id="email" name="email" value="{{$data->email}}" readonly>
        
        <br>
        <label for="email">Subject</label>
        <input type="text" id="subject" name="subject" >
        @error('subject')<span style="color:red">{{$message}}</span>@enderror
        <br>
        <label for="subject">Email Content</label>
        <textarea id="content" name="content" placeholder="Write something.." style="height:170px"></textarea>
        @error('content')<span style="color:red">{{$message}}</span>@enderror<br>
        <input type="submit" value="Submit" name="mail" value="yes">
      </form>
    </div>
  </div>
</div>
@stop