@extends('layout.layout')

@section('title','Featured Images')

@section('content')

    <b><br>Add Nearby Places for {{$data->title}}
        <hr>
        @if (Session::get('success'))
        <div class="alert alert-success alert-dismissible">
            <button style="color:#fff" type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

            {{Session::get('success')}}
        </div>
        @elseif (Session::get('warning'))
        <div class="alert alert-danger alert-dismissible">
            <button style="color:#fff" type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

            {{Session::get('warning')}}
        </div>
        @endif


        <form method="post"  action="{{route('placeAdd')}}" id="placeForm">
            @csrf
         <div class="row">
           <div class="col-md-6">
            <div class="form-group">
                <label for="exampleInputEmail1">Nearby Place</label>
                <input type="text" name="place" id="place" class="form-control" placeholder="Enter nearby place">
                <span style='color:red' id="image-input-error">
                    @error('place'){{$message}} @enderror
                </span>
               
            </div>
            </div>
        </div>
        <input type="hidden" name="propertyId" id="propertyId" value="{{$data->propertyId}}">
        
            <button type="submit" class="btn btn-success" name='isPlace' value='yes'>Add</button>
        </form>
       

       <!--- Listing of featured properties-->

       <table class="table">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Nearby Place</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>@php $n=1; @endphp
  @foreach($places as $place)
    <tr>
      <th scope="row">{{$n++}}</th>
      <td>{{$place->placeLocation}}</td>
      <td> 
        <a href="{{route('placedelete',base64_encode($place->placeId))}}">
        <i class="fa fa-trash"></i></a>
      </td>
    </tr>
    @endforeach
    
  </tbody>
</table>

@stop