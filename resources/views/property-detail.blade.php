@extends('layout.layout')

@section('title','Property Detail')

@section('content')
<style>
* {
  box-sizing: border-box;
}

/* Style inputs */
input[type=text], select, textarea {
  width: 100%;
  padding: 12px;
  border: 1px solid #ccc;
  margin-top: 6px;
  margin-bottom: 16px;
  resize: vertical;
}

input[type=submit] {
  background-color: #04AA6D;
  color: white;
  padding: 12px 20px;
  border: none;
  cursor: pointer;
}

input[type=submit]:hover {
  background-color: #45a049;
}

/* Style the container/contact section */
.container {
  border-radius: 5px;
  background-color: #f2f2f2;
  padding: 10px;
}

/* Create two columns that float next to eachother */
.column {
  float: left;
  width: 50%;
  margin-top: 6px;
  padding: 20px;
}

/* Clear floats after the columns */
.row:after {
  content: "";
  display: table;
  clear: both;
}

/* Responsive layout - when the screen is less than 600px wide, make the two columns stack on top of each other instead of next to each other */
@media screen and (max-width: 600px) {
  .column, input[type=submit] {
    width: 100%;
    margin-top: 0;
  }
}
* {box-sizing: border-box}
body {font-family: Verdana, sans-serif; margin:0}

/* Slideshow container */
.slideshow-container {
  position: relative;
  background: #f1f1f1f1;
}

/* Slides */
.mySlides {
  display: none;
  padding: 80px;
  text-align: center;
}

/* Next & previous buttons */
.prev, .next {
  cursor: pointer;
  position: absolute;
  top: 50%;
  width: auto;
  margin-top: -30px;
  padding: 16px;
  color: #888;
  font-weight: bold;
  font-size: 20px;
  border-radius: 0 3px 3px 0;
  user-select: none;
}

/* Position the "next button" to the right */
.next {
  position: absolute;
  right: 0;
  border-radius: 3px 0 0 3px;
}

/* On hover, add a black background color with a little bit see-through */
.prev:hover, .next:hover {
  background-color: rgba(0,0,0,0.8);
  color: white;
}

/* The dot/bullet/indicator container */
.dot-container {
    text-align: center;
    padding: 20px;
    background: #ddd;
}

/* The dots/bullets/indicators */
.dot {
  cursor: pointer;
  height: 15px;
  width: 15px;
  margin: 0 2px;
  background-color: #bbb;
  border-radius: 50%;
  display: inline-block;
  transition: background-color 0.6s ease;
}

/* Add a background color to the active dot/circle */
.active, .dot:hover {
  background-color: #717171;
}

/* Add an italic font style to all quotes */
q {font-style: italic;}

/* Add a blue color to the author */
.author {color: cornflowerblue;}

</style>
<div class="container">
  <div style="text-align:center">
    <h2>Property Details</h2>
    
  </div>
  <div class="row">
    <div class="column">
      <table  class="table">
      <tr><td><b>Property Title : </b></td><td>{{$data->title}}</td></tr>
      <tr><td><b>Floor Area(Sqft) : </b></td><td>{{$data->area}}</td></tr>
      <tr><td><b>Price : </b></td><td>{{$data->price}}</td></tr>
      <tr><td><b>Bedroom : </b></td><td>{{$data->bedroom}}</td></tr>
      <tr><td><b>Bathroom : </b></td><td>{{$data->bathroom}}</td></tr>
      <tr><td><b>Address : </b></td><td>{{$data->address}}</td></tr>
      <tr><td><b>City: </b></td><td>{{getCityName($data->city)}}</td></tr>
      <tr><td><b>Address : </b></td><td>{{$data->address}}</td></tr>
      <tr><td><b>Description : </b></td><td>{{$data->description}}</td></tr>
      </table>
    </div>
    <div class="column">
    @if (Session::get('success'))
        <div class="alert alert-success alert-dismissible">
            <button style="color:#fff" type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

            {{Session::get('success')}}
        </div>
    @endif
      <form action="{{route('home.store')}}" method="post" id="messageForm">
      @csrf
      <input type="hidden" name="person" value="{{$data->addedBy}}">
      <input type="hidden" name="property" value="{{$data->propertyId}}">

        <label for="name">Name</label>
        <input type="text" id="name" name="name" placeholder="Your name..">
        <br>
        <label for="email">Email</label>
        <input type="text" id="email" name="email" placeholder="Your email..">
        <br>
        <label for="contact">Phone</label>
        <input type="text" id="phone" name="phone" placeholder="Your mobile.." maxlength="10">
        <br>
        <label for="subject">Subject</label>
        <textarea id="message" name="message" placeholder="Write something.." style="height:170px"></textarea>
        <input type="submit" value="Submit" name="enquiry" value="yes">
      </form>
    </div>
  </div>
</div>

@php $feature=$data->feature; @endphp
<div class="slideshow-container">
@foreach($feature as $fimage)
<div class="mySlides">
  <img src='{{asset("storage/uploads/$fimage->image")}}' width="500px">
</div>
@endforeach
<a class="prev" onclick="plusSlides(-1)">❮</a>
<a class="next" onclick="plusSlides(1)">❯</a>

</div>

<div class="dot-container">
  <span class="dot" onclick="currentSlide(1)"></span> 
  <span class="dot" onclick="currentSlide(2)"></span> 
  <span class="dot" onclick="currentSlide(3)"></span> 
</div>

<script>
var slideIndex = 1;
showSlides(slideIndex);

function plusSlides(n) {
  showSlides(slideIndex += n);
}

function currentSlide(n) {
  showSlides(slideIndex = n);
}

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("dot");
  if (n > slides.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";  
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";  
  dots[slideIndex-1].className += " active";
}
</script>

@stop