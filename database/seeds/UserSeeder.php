<?php


use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use App\User;


class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user=new User;
        $user->userName='Property Agent';
        $user->userEmail='agent@gmail.com';
        $user->password=base64_encode(Str::random(7));
        $user->userRole=1;
        $user->save();
    }
}
