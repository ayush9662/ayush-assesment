<?php

use Illuminate\Database\Seeder;
use App\City;

class Cityseeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $city = ["Lucknow","Noida","Mumbai","Pune","New Mumbai","Ahemdabad","Kokatta","Indore","Banglore","Hyderabad","Kochi"];

        for ($item=0; $item < count($city); $item++) 
        { 
            City::create([
                "cityName"=>$city[$item]

            ]);
          

        }
    
}
}
