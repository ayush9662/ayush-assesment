<?php

namespace App\Repositories;

use App\User;


class Authrepo
{

    //get login details and verification
    public static function loginVerify($data)
    {
        $find = User::where(['userEmail' => $data['email'], "password" => base64_encode($data['password'])])->first();
        return $find;
    }

    //make user entry
    public static function userEntry($data)
    {
        if (isset($data['isSignup'])) {
            $user = new User;
            $user->userName = $data['name'];
            $user->userEmail = $data['email'];
            $user->userRole = $data['role'];
            $user->password = base64_encode($data['password']);
            $user->save();
            return $user;
        }
    }

    //email check
    public static function uniqueEmail($email)
    {
        if (!empty($email)) {

            $record = User::where('userEmail', $email)->first();
            if ($record) {
                $error_message= "An account already exists with this email";
            } else {
                $error_message=" ";
            }
        } else {
            $error_message= " ";
        }
        return $error_message;
    }
   
}
