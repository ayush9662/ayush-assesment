@extends('layout.layout')

@section('title','Featured Images')

@section('content')

    <b><br>Add Featured Images for {{$data->title}}
        <hr>
        @if (Session::get('success'))
        <div class="alert alert-success alert-dismissible">
            <button style="color:#fff" type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

            {{Session::get('success')}}
        </div>
        @elseif (Session::get('warning'))
        <div class="alert alert-danger alert-dismissible">
            <button style="color:#fff" type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

            {{Session::get('warning')}}
        </div>
        @endif


        <form method="post"  action="javascript:void(0)" id="imageForm" enctype="multipart/form-data">
            @csrf
         <div class="row">
           <div class="col-md-6">
            <div class="form-group">
            File Notes:
            <br>
            1. File should be .jpeg/.jpg/.png<br>
            2. File size should not be greater than 3 MB.<br>
            3. File field is required.<br><br><hr>
                <label for="exampleInputEmail1">Featured Image</label>
                <input type="file" name="featured" id="featured" class="form-control" accept="image/x-png,image/jpg,image/jpeg">
                <span style='color:red' id="image-input-error">
                    @error('featured'){{$message}} @enderror
                </span>
               
            </div>
            </div>
        </div>
        <input type="hidden" name="propertyId" id="propertyId" value="{{$data->propertyId}}">
        
            <button type="submit" class="btn btn-success" name='isFeatured' value='yes'>Upload</button>
        </form>
       

       <!--- Listing of featured properties-->

       <table class="table">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Image</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>@php $n=1; @endphp
  @foreach($images as $image)
    <tr>
      <th scope="row">{{$n++}}</th>
      <td><a href='{{asset("storage/uploads/$image->image")}}' target="_blank">View</a></td>
      <td> 
        <a href="{{route('delete',base64_encode($image->featuredImageId))}}">
        <i class="fa fa-trash"></i></a>
      </td>
    </tr>
    @endforeach
    
  </tbody>
</table>

<script>
    

$(document).ready(function (e) {
   $('#imageForm').submit(function(e) {
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
       e.preventDefault();
       let formData = new FormData(this);
       $('#image-input-error').text('');
       
       $.ajax({
          type:'POST',
          url: "{{url('/upload-featured')}}",
           data: formData,
           contentType: false,
           processData: false,
           success: (response) => {
             if (response) {
               this.reset();
               alert('Image has been uploaded successfully');
               window.location.reload();
             }
           },
           error: function(response){
             
                $('#image-input-error').text("Please check with file notes");
           }
       });
  });
});
</script>

@stop