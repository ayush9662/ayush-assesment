<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Property extends Model
{
    //
    public function feature()
    {
        return $this->hasMany('App\Featuredimage','featurePropertyId','propertyId');
    }  

    public function gallery()
    {
        return $this->hasMany('App\Galleryimage','galleryPropertyId','propertyId');
    }  
    public function place()
    {
        return $this->hasMany('App\Place','placeId','propertyId');
    }  
}
