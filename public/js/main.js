

/*       Sidebar Menu
-------------------------*/
$(function () {
    setTimeout(function () {
        $(".alert.auto-hide").fadeOut(500);
    }, 5000);

   
    
    $(".toggle-password").click(function () {
        var input = $($(this).data("target"));
        if (input.attr("type") === "password") {
            input.attr("type", "text");
            $(this).find(".eye-open").hide();
            $(this).find(".eye-close").show();
        } else {
            input.attr("type", "password");
            $(this).find(".eye-open").show();
            $(this).find(".eye-close").hide();
        }
    });

    $("#profile_pic").change(function () {
        readURL(this, ".profile-photo img");
    });
    // Dropdown Menu
    $("[data-toggle]").on("click", function (e) {
        e.stopPropagation();
        $(".dropdown-content").removeClass("show");
        $(this).parent().find(".dropdown-content").toggleClass("show");
    });
    $(document).click(function () {
        $(".dropdown-content").removeClass("show");
    });

    $("[data-toggle]").on("click", function (e) {
        e.stopPropagation();
        $(".member-content").removeClass("show");
        $(this).parent().find(".member-content").toggleClass("show");
    });
    $(document).click(function (e) {
        if (!$(e.target).closest(".member-dropdown").length) {
            $(".member-content").removeClass("show");
        }
    });

    //Validation for alphanumeric name

    jQuery.validator.addMethod("alphaNumeric", function(value, element) {
        return this.optional(element) || /^[a-zA-Z0-9 ]{2,255}$/.test(value);
      }, "Please enter a valid name.");
    
      //validation for Email
      jQuery.validator.addMethod("customEmail", function(value, element) {
        return this.optional(element) || /^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/.test(value);
      }, "Please enter a valid Email.");



    //Login Form Validation
    var _loginForm = $("#loginForm");
    if (_loginForm.length > 0) {
        _loginForm.validate({
            rules: {
                email: {
                    required: true,
                    customEmail: true,
                },
                password: {
                    required: true,
                },
            },
            messages: {
                password: {
                    required: "The password field is required.",
                },
                email: {
                    required: "The email field is required.",
                   customEmail: "Please enter a valid email address.",
                },
            },
            submitHandler: function (form) {
                form.submit();
            },
        });
    }

  
//validation for register form
    var _registerForm = $("#registerForm");
    if (_registerForm.length > 0) {
        _registerForm.validate({
            rules: {
                password: {
                    required: true,
                    minlength: 6,
                },
                name: {
                    required: true,
                    alphaNumeric:true,
                },
                email:{
                    required:true,
                    customEmail: true,
                },
                role:{
                    required:true,
                }
            },
            messages: {
                password: {
                    required: "The password field is required.",
                    minlength:
                        "The password must be at least 6 characters long.",
                },
                name: {
                    required: "The name field is required.",
                    
                    
                },
                email: {
                    required: "The email field is required.",
                    customEmail: "Please enter a valid email address.",
                },
                role: {
                    required: "Please select your role",
                },
            },
            submitHandler: function (form) {
                form.submit();
            },
        });
    }

    //Validation for prilfe form
    var _profileForm = $("#profileForm");
    if (_profileForm.length > 0) {
        _profileForm.validate({
            rules: {
                password: {
                    required: true,
                    minlength: 6,
                },
                name: {
                    required: true,
                    alphaNumeric:true,
                },
               
            },
            messages: {
                password: {
                    required: "The password field is required.",
                    minlength:
                        "The password must be at least 6 characters long.",
                },
                name: {
                    required: "The name field is required.",
                },
            },
            submitHandler: function (form) {
                form.submit();
            },
        });
    }
    

     //Validation for nearby palce 
     var _placeForm = $("#placeForm");
     if (_placeForm.length > 0) {
         _placeForm.validate({
             rules: {
                 place: {
                     required: true,
                 },
                
             },
             messages: {
                place: {
                     required: "Nearby place field is required",
                   
                 },
                
             },
             submitHandler: function (form) {
                 form.submit();
             },
         });
     }
     
    //Validation for message form

    var _messageForm = $("#messageForm");
    if (_messageForm.length > 0) {
        _messageForm.validate({
            rules: {
                name: {
                    required: true,
                    alphaNumeric:true,
                },
                email: {
                    required: true,
                    customEmail: true,
                },
                phone: {
                    required: true,
                    digits: true,
                    minlength: 10,
                    maxlength: 10,
                },
                message:{
                    required:true
                }
            },
            messages: {
                name: {
                    required: "The name field is required.",
                },
                email: {
                    required: "The email field is required.",
                    customEmail: "Please enter a valid email address.",
                },
                phone: {
                    required: "The phone field is required.",
                    digits: "Please enter only number.",
                    minlength: "Please enter must be 10 characters.",
                    maxlength: "Please enter must be 10 characters.",
                },
                message:{
                    required:"The message field is required",
                }
            },
            submitHandler: function (form) {
                form.submit();
            },
        });
    }


//Validation for property form
var _propertyForm = $("#propertyForm");
if (_propertyForm.length > 0) {
    _propertyForm.validate({
        rules: {
            title: {
                required: true,
            },
            area: {
                required: true,
            },
            price: {
                required: true,
                digits: true,
            },
            bedroom:
            {
                required: true,
                digits: true,
            },
            bathroom:
            {
                required: true,
                digits: true,
            },
            city: {
                required: true,
            },
            address: {
                required: true,
            },
            description: {
                required: true,
            },
            
        },
        messages: {
            title: {
                required: "Title field is required.",
                alphaNumeric:true,
            },
            area: {
                required: "Floor area field is required",
            },
            price: {
                required: "Price field is required.",
                digits: "Please enter only numeric value.",
               
            },
            bedroom:{
                required: "No of bedroom is required.",
                digits: "Please enter only numeric value.",
            },
            bedroom:{
                required: "No of bathroom is required.",
                digits: "Please enter only numeric value.",
            },
            city: {
                required: "City field is required.",
            },
            address: {
                required: "Address field is required.",
            },
            description: {
                required: "Description field is required.",
            },
           
        },
        submitHandler: function (form) {
            form.submit();
        },
    });
}


//Validation for filter form

var _filterForm = $("#filterForm");
if (_filterForm.length > 0) {
    _filterForm.validate({
        rules: {
            // title: {
            //     required: true,
            //     alphaNumeric:true,
            // },
            city: {
                required: true,
            },
            bedroom: {
                required: true,
                digits: true,
            },
            price:{
                required:true,
                digits: true,
            }
        },
        messages: {
            // title: {
            //     required: "The property name field is required.",
            // },
            city: {
                required: "Please select a city name.",
            },
            bedroom: {
                required: "No of bedroom is required.",
                digits: "Please enter only numeric values.",
            },
            price:{
                required:"The max price field is required",
                digits: "Please enter only numeric values.",
            }
        },
        submitHandler: function (form) {
            form.submit();
        },
    });
}



});
