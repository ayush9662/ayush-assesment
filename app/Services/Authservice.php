<?php

namespace App\Services;

use App\Repositories\Authrepo;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;
// use Illuminate\Support\Facades\Log;
// use InvalidArgumentException;

class Authservice
{
    //login validation
    public static function loginValidate($data)
    {
        Validator::make($data,[
            "email"=>"required|email",
            "password"=>"required"
        ])->validate();

        $record=Authrepo::loginVerify($data);
        return $record;



    }

    //registration
    public static function getRegistered($data)
    {
        Validator::make($data,[
            "name"=>'required',
            "email"=>"required|email|unique:users,userEmail",
            "role"=>"required",
            "password"=>"required|min:6"
        ])->validate();
        //transaction
        DB::beginTransaction();
        try{
            $record=Authrepo::userEntry($data);
            DB::commit();
            return $record;
        }
        catch(\Exception $e)
        {
           DB::rollBack();
           throw $e;
        }
        
        
        

    }

    //email existing check
    public static function getExistingUser($email)
    {
       return Authrepo::uniqueEmail($email);
    }
}
