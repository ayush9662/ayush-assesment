<?php

namespace App\Repositories;

use App\User;
use Illuminate\Support\Facades\Session;

class Profilerepo
{

    //Get profile data
    public static function profileData($id)
    {
        $user=User::where('userId',$id)->first();
        return $user;
    }

    //Update user record
    public static function updateProfile($data,$id)
    {
        if(isset($data['file']))
        {
            $file=$data['file'];
            $imageName=$file->getClientOriginalName();
            $updateData=[
                'userName'=>$data['name'],
                'password'=>base64_encode($data['password']),
                'userImage'=>$imageName
            ];
        }
        else{
            $updateData= [
                'userName'=>$data['name'],
                'password'=>base64_encode($data['password']),
            ];
        }
        $update=User::where('userId',$id)->update($updateData);
        Session::put('username',$data['name']);
        return $update;
      
         
    }

}