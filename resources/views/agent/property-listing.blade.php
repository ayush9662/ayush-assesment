@extends('layout.layout')

@section('title','Property Listing')

@section('content')

  
      
        @if (Session::get('success'))
        <div class="alert alert-success alert-dismissible">
            <button style="color:#fff" type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

            {{Session::get('success')}}
        </div>
        @elseif (Session::get('warning'))
        <div class="alert alert-danger alert-dismissible">
            <button style="color:#fff" type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

            {{Session::get('warning')}}
        </div>
        @endif
        <hr>
        <a href="{{route('property.create')}}">Add Properties</a><br><br>
        <table class="table">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Property Name</th>
      <th scope="col">Price</th>
      <th scope="col">Featured Images</th>
      <th scope="col">Gallery Images</th>
      <th scope="col">Neary By Places</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>@php $n=1; @endphp
  @foreach($data as $proeprty)
    <tr>
      <th scope="row">{{$n++}}</th>
      <td>{{$proeprty->title}}</td>
      <td>{{$proeprty->price}}</td>
      <td><a href="{{route('featured',base64_encode($proeprty->propertyId))}}">Upload</a></td>
      <td><a href="{{route('gallery',base64_encode($proeprty->propertyId))}}">Upload</a></td>
      <td><a href="{{route('place',base64_encode($proeprty->propertyId))}}">Add</a></td>
      <td> 
        <a href="{{route('property.show',base64_encode($proeprty->propertyId))}}"> <i class="fa fa-eye"></i></a>
        <a href="{{route('property.edit',base64_encode($proeprty->propertyId))}}"><i class="fa fa-edit"></i></a>
        <form action="{{route('property.destroy',base64_encode($proeprty->propertyId))}}" method="post">
        @csrf
        @method('DELETE')
        <button type="submit" class="text-danger" title="Delete">
        <i class="fa fa-trash"></i></button>
        </form>
      </td>
    </tr>
    @endforeach
    
  </tbody>
</table>
{{$data->links()}}
@stop