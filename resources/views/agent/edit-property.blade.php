@extends('layout.layout')

@section('title','Edit Property')

@section('content')

    <b><br>Edit your property.
        <hr>
        @if (Session::get('success'))
        <div class="alert alert-success alert-dismissible">
            <button style="color:#fff" type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

            {{Session::get('success')}}
        </div>
        @elseif (Session::get('warning'))
        <div class="alert alert-danger alert-dismissible">
            <button style="color:#fff" type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

            {{Session::get('warning')}}
        </div>
        @endif

        <form action="{{route('property.update',base64_encode($data->propertyId))}}" method='post' id="propertyForm">
        @method('PUT')
            @csrf
         <div class="row">
           <div class="col-md-6">
            <div class="form-group">
                <label for="exampleInputEmail1">Property Title</label>
                <input type="text" name="title" id="title" class="form-control" placeholder="Enter property title" value="{{$data->title}}">
                <span style='color:red'>
                    @error('title'){{$message}} @enderror
                </span>
            </div>
            </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="exampleInputEmail1">Price</label>
                <input type="text" name="price" id="price" class="form-control" placeholder="Enter property price" value="{{$data->price}}">
                <span style='color:red'>
                    @error('price'){{$message}}@enderror
                </span>
            </div>
        </div>
        </div>
        <div class="row">
           <div class="col-md-6">
            <div class="form-group">
                <label for="exampleInputEmail1">Floor Area(In SQFT)</label>
                <input type="text" name="area" id="area" class="form-control" placeholder="Enter floor area" value="{{$data->area}}">
                <span style='color:red'>
                    @error('area'){{$message}} @enderror
                </span>
            </div>
            </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="exampleInputEmail1">No of Bedrooms</label>
                <input type="number" name="bedroom" id="bedroom" class="form-control" placeholder="Enter no of bedrooms" min="1" step="1" value="{{$data->bedroom}}">
                <span style='color:red'>
                    @error('bedroom'){{$message}}@enderror
                </span>
            </div>
        </div>
        </div>
        <div class="row">
           <div class="col-md-6">
           <div class="form-group">
                <label for="exampleInputEmail1">No of Bathrooms</label>
                <input type="number" name="bathroom" id="bathroom" class="form-control" placeholder="Enter no of bedrooms" min="1" step="1" value="{{$data->bathroom}}">
                <span style='color:red'>
                    @error('bathroom'){{$message}}@enderror
                </span>
            </div>
            
            </div>
        <div class="col-md-6">
        <div class="form-group">
                <label for="exampleInputEmail1">City</label>
                <select name="city" id="city" class="form-control">
                <option value="">--Select City--</option>
                @foreach($city as $cities)
                <option value="{{$cities->cityId}}" {{$cities->cityId==$data->city?'selected':''}}>{{$cities->cityName}}</option>
                @endforeach
                </select>
                  <span style='color:red'>
                    @error('city'){{$message}} @enderror
                </span>
            </div>
        </div>
        </div>
        <div class="row">
           <div class="col-md-6">
            <div class="form-group">
                <label for="exampleInputEmail1">Address</label>
                <input type="text" name="address" id="address" class="form-control" placeholder="Enter property address" value="{{$data->address}}">
                <span style='color:red'>
                    @error('address'){{$message}} @enderror
                </span>
            </div>
            </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="exampleInputEmail1">Description</label>
                <textarea class='form-control' name="description" id="description">{{$data->description}}</textarea>
                <span style='color:red'>
                    @error('description'){{$message}}@enderror
                </span>
            </div>
        </div>
        </div>
            <button type="submit" class="btn btn-success" name='updateProperty' value='yes'>Update Property</button>
        </form>
       

@stop