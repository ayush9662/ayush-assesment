@extends('layout.layout')

@section('title','Registeration Page')

@section('content')
<div class='col-sm-6'>
    <b><br>Signup to your account
        <hr>
        @if (Session::get('success'))
        <div class="alert alert-success alert-dismissible">
            <button style="color:#fff" type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

            {{Session::get('success')}}
        </div>
        @elseif (Session::get('warning'))
        <div class="alert alert-danger alert-dismissible">
            <button style="color:#fff" type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

            {{Session::get('warning')}}
        </div>
        @endif
        <div>
            <form action="{{route('authenticate.store')}}" method='post' id="registerForm">
                @csrf
                <div class="form-group">
                    <label for="exampleInputEmail1">Name</label>
                    <input type="text" name="name" id="name" class="form-control" placeholder="Enter your name" value="{{old('email')}}">
                    <span style='color:red'>
                        @error('name'){{$message}} @enderror
                    </span>
                </div>
                <div class="form-group">
                    <label for="exampleInputEmail1">Email</label>
                    <input type="email" name="email" id="email" class="form-control" placeholder="Enter your email" value="{{old('email')}}">
                    <span style='color:red' id="email-error">
                        @error('email'){{$message}} @enderror
                    </span>
                </div>

                <div class="form-group">
                    <label for="exampleInputEmail1">Password</label>
                    <input type="password" name="password" id="password" class="form-control" placeholder="Enter your password">
                    <span style='color:red'>
                        @error('password'){{$message}}@enderror
                    </span>
                </div>

                <div class="form-group">
                    <label for="exampleInputEmail1">Role</label>
                    <select name="role" id="role" class="form-control">
                        <option value="">--Select Role--</option>
                        <option value="1">Agent</option>
                        <option value="2">Customer</option>
                    </select>
                    <span style='color:red'>
                        @error('password'){{$message}}@enderror
                    </span>
                </div>
                <button type="submit" class="btn btn-success" name='isSignup' value='yes'>Register</button>
            </form>
        </div>
        <hr>
        <span> <a href="{{route('authenticate.index')}}" class="authentication">Login to your account</a></span>


</div>
<script>
$(document).ready(function() {
  
   $("#email").keyup(function() {
      
       var email = $('#email').val();
       //Validating, if "email" is empty.
       if (email == "") {
           
           $("#email-error").html("");
       }
       //If name is not empty.
       else {
           //AJAX is called.
           $.ajax({
               type: "GET",
               url: "{{url('/authenticate')}}/"+ email,
               
            //    data: {
            //        search: email
            //    },
            dataType: "html",                  
            success: function(data){                    
            $("#email-error").html(data); 
           
        }
        
           });
        }
   });
});


</script>

@stop