<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePropertiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('properties', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('propertyId')->autoIncrement();
            $table->string('title')->nullable();
            $table->string('price')->nullable();
            $table->string('area')->nullable();
            $table->integer('bedroom')->nullable();
            $table->integer('bathroom')->nullable();
            $table->integer('city');
            $table->index('city');
            $table->foreign('city')->references('cityId')->on('cities');
            $table->string('address')->nullable();
            $table->longText('description')->nullable();
            $table->boolean('isDeleted')->default(0);
            $table->integer('addedBy')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('properties');
    }
}
