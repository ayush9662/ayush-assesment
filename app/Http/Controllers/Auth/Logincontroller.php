<?php

namespace App\Http\Controllers\Auth;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\Authservice;
use Illuminate\Support\Facades\Session;

class Logincontroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view("auth.login");
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view("auth.register");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $data = $request->all();
        $registered = Authservice::getRegistered($data);
        if ($registered) {
            return redirect()->route('authenticate.index')->with('success', 'Congratulations! Your account is created successfully');
        }
        return redirect()->route('authenticate.create')->with('success', "$registered");
    }

    //verify login details
    public function login(Request $request)
    {
        $data=$request->all();
        $checkRecord=Authservice::loginValidate($data);
        if($checkRecord)
        {
            $route=$checkRecord->userRole==1?'agent.index':'customer.index';
            Session::put(['userid'=>$checkRecord->userId,'username'=>$checkRecord->userName,'role'=>$checkRecord->userRole]);
            return redirect()->route("$route")->with('success', 'Welcome Back!');
        
        }
        return redirect()->route('authenticate.index')->with('warning', 'Invalid username & password');
        
    }

    //user logout
    public function logout()
    {
        Session::forget(['role','username','userid']);
        Session::flush();
        return redirect()->route('authenticate.index')->with('success','You have been logout successfully');

    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($email)
    {
        //
       $exist=Authservice::getExistingUser($email);
       return $exist;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
