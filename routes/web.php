<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Auth routes.
Route::resource('/authenticate','Auth\Logincontroller')->middleware(['back','path']);
Route::post('/login-verify','Auth\Logincontroller@login')->name('login')->middleware(['back','path']);
Route::resource('/home','Home\Homecontroller');


//Agent  routes
Route::group(['middleware'=>['web','access','agent','back']],function(){
    Route::resource('/agent','Agent\Agentcontroller');
    Route::resource('/property','Property\Basicinfo');
    Route::get('/featured-images/{id}','Property\FeatureImages@index')->name('featured');
    Route::post('/upload-featured','Property\FeatureImages@imageUpload');
    Route::get('/featured-delete/{id}','Property\FeatureImages@delete')->name('delete');
    Route::get('/gallery-images/{id}','Property\GalleryImages@index')->name('gallery');
    Route::post('/upload-gallery','Property\GalleryImages@imageUpload');
    Route::get('/gallery-delete/{id}','Property\GalleryImages@delete')->name('gallerydelete');
    Route::get('/near-place/{id}','Property\Nearplace@index')->name('place');
    Route::post('/add-place','Property\Nearplace@addPlace')->name('placeAdd');
    Route::get('/place-delete/{id}','Property\Nearplace@delete')->name('placedelete');
});

//Logout 
Route::group(['middleware'=>['web','access','back']],function(){
    Route::get('/logout','Auth\Logincontroller@logout')->name('logout');
});

//Customer routes
Route::group(['middleware'=>['web','access','customer','back']],function(){
    Route::resource('/customer','Customer\Customercontroller');
});

