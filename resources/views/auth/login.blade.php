@extends('layout.layout')

@section('title','Login Page')

@section('content')
<div class='col-sm-6'>
    <b><br>Login to your account
        <hr>
        @if (Session::get('success'))
        <div class="alert alert-success alert-dismissible">
            <button style="color:#fff" type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

            {{Session::get('success')}}
        </div>
        @elseif (Session::get('warning'))
        <div class="alert alert-danger alert-dismissible">
            <button style="color:#fff" type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

            {{Session::get('warning')}}
        </div>
        @endif

        <form action="{{route('login')}}" method='post' id="loginForm">
            @csrf
            <div class="form-group">
                <label for="exampleInputEmail1">Email</label>
                <input type="email" name="email" id="email" class="form-control" placeholder="Enter your email" value="{{old('email')}}">
                <span style='color:red'>
                    @error('email'){{$message}} @enderror
                </span>
            </div>

            <div class="form-group">
                <label for="exampleInputEmail1">Password</label>
                <input type="password" name="password" idd="password" class="form-control" placeholder="Enter your password">
                <span style='color:red'>
                    @error('password'){{$message}}@enderror
                </span>
            </div>
            <button type="submit" class="btn btn-success" name='isLogin' value='yes'>Login</button>
        </form>
        <hr>
        <span> <a href="{{route('authenticate.create')}}" class="authentication">Create new account</a></span>

</div>


@stop