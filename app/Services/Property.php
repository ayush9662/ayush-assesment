<?php

namespace App\Services;

use App\Repositories\Propertyrepo;
use Illuminate\Support\Facades\Validator;

class Property
{
    //adding a new property
    public static function addProperty($data)
    {
        if(isset($data['addProperty']))
        {
            Validator::make($data,[
                'title'=>'required',
                'price'=>'required|numeric',
                'area'=>'required',
                'bedroom'=>'required|numeric',
                'bathroom'=>'required|numeric',
                'city'=>'required',
                'address'=>'required',
                'description'=>'required'
    
            ]);
           return Propertyrepo::insertProperty($data);
        }
        


    }

    //get all properties 
    public static function getProperty()
    {
        $property=Propertyrepo::getActiveProperty();
        return $property;
        
    }

    // get edit form for property
    public static function getPropertyForm($id)
    {
        $id=base64_decode($id);
        return Propertyrepo::getPropertyFormValues($id);

    }

    //update property data
    public static function updatePropertyData($data,$id)
    {
        if(isset($data['updateProperty']))
        {
            $id=base64_decode($id);
            Validator::make($data,[
                'title'=>'required',
                'price'=>'required|numeric',
                'area'=>'required',
                'bedroom'=>'required|numeric',
                'bathroom'=>'required|numeric',
                'city'=>'required',
                'address'=>'required',
                'description'=>'required'
    
            ]);
            return Propertyrepo::updatePropertyWithId($data,$id);

        }
     
    }

    //dost delete property with id
    public static function softDeleteProperty($id)
    {
        $id=base64_decode($id);
        return Propertyrepo::deleteProperty($id);
    }

    //get active preoperties for front
    public static function frontProperties($data)
    {
        return Propertyrepo::getFrontProperties($data);

    } 

    //get property details for front
    public static function getFrontDetails($id)
    {
        $id=base64_decode($id);
        return Propertyrepo::getFrontDetailsFromDB($id);

    }

    //store enquiry message for agent
    public static function addEnquiry($data)
    {
        if(isset($data['enquiry']))
        {
            Validator::make($data,[
                'name'=>'required',
                'email'=>'required|email',
                'phone'=>'required',
                'message'=>'required',
    
            ]);
            return Propertyrepo::setEnquiry($data);
        }
    }
}