<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Session;

class Path
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $path=$request->path();
        if(Session::get('userid') && $path=='authenticate')
        {
            return redirect()->back();

        }
        elseif(Session::get('userid') && $path=='authenticate/create')
        {
            return redirect()->back();

        }
        return $next($request);
    }
}
