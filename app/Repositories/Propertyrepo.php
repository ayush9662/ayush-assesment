<?php

namespace App\Repositories;

use App\Property;
use App\Message;
use Illuminate\Support\Facades\Session;


class Propertyrepo
{
    //new entry for property
    public static function insertProperty($data)
    {
        $property=new Property;
        $property->title=$data['title'];
        $property->price=$data['price'];
        $property->area=$data['area'];
        $property->bedroom=$data['bedroom'];
        $property->bathroom=$data['bathroom'];
        $property->city=$data['city'];
        $property->address=$data['address'];
        $property->description=$data['description'];
        $property->addedBy=Session::get('userid');
        $saved=$property->save();
        return $saved;

    }
    //get not deleted properties
    public static function getActiveProperty()
    {
        return Property::where(['isDeleted'=>0,"addedBy"=>Session::get('userid')])->paginate(10);
    }

    //get edit form values by id
    public static function getPropertyFormValues($id)
    {
        $record=Property::where('propertyId',$id)->first();
        return $record;
    }

    //updating property details
    public static function updatePropertyWithId($data,$id)
    {
        
        $updateData=[
            "title"=>$data['title'],
           "price"=>$data['price'],
            "area"=>$data['area'],
           "bedroom"=>$data['bedroom'],
           "bathroom"=>$data['bathroom'],
           "city"=>$data['city'],
            "address"=>$data['address'],
           "description"=>$data['description']

        ];
        $updated=Property::where('propertyId',$id)->update($updateData);
           return $updated;
    }
   
    //delete property
    public static function deleteProperty($id)
    {
        $find=Property::where('propertyId',$id)->first();
        if($find)
        {
            $deleted=Property::where('propertyId',$id)->update(['isDeleted'=>1]);
            return $deleted;
        }
        return false;
    }

    public static function getFrontProperties($data)
    {
        if(count($data)==0)
        {
            $property=Property::where('isDeleted',0)->get();
        }
        else
        {
            $property=Property::where(['city'=>$data['city'],["price","<=",$data['price']],"bedroom"=>$data['bedroom']])->get();

        }
        return $property;
    }

    public static function getFrontDetailsFromDB($id)
    {
        $find=Property::with('feature','gallery','place')->where('propertyId',$id)->first();
        if($find)
        {
            return $find;
        }
        return false;
    }

    public static function setEnquiry($data)
    {
        $enquiry=new Message;
        $enquiry->name=$data['name'];
        $enquiry->email=$data['email'];
        $enquiry->contact=$data['phone'];
        $enquiry->message=$data['message'];
        $enquiry->enquiredUser=$data['person'];
        $enquiry->enquiredProperty=$data['property'];
        $store=$enquiry->save();
        return $store;
    }

}