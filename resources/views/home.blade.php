
@extends('layout.layout')

@section('title','Home Page')

@section('content')
<div class="site-blocks-cover overlay d-none d-md-block" id="home-section">
    <div class="container">
        <div class="row align-items-center justify-content-center">
            <div class="col-md-6 mt-lg-5 text-left">
                <h1 class="h3 mb-5">
                    Welcome to Realstate
                </h1>
                <div>
                    <a href="{{route('authenticate.create')}}" class="btn smoothscroll btn-success mr-2 mb-2 px-5">Join Now</a>
                </div>
            </div>
            <div class="col-md-6">
                <form action="{{route('home.index')}}" class="filter-form" method="get" id="filterForm">
                @csrf
                    <br><br>
                    <h1 class="h3 mb-5">Make your search fast</h1>
                    <div class="row">
                    <div class="col-md-12">
                    <input type="hidden" name="ayush" value="10">
                            <div class="form-group">
                                <select class="form-control" name="city" id="city">
                                    <option value="">--Select City--</option>
                                    @foreach($cities as $city)
                                     <option value="{{$city->cityId}}">{{$city->cityName}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="form-group">
                                <input  type="number" name="bedroom" id="bedroom" class="form-control" min="1" step="1" placeholder="No of bedrooms" />
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <input id="price" name="price" class="form-control"  min="1" step="1" type="number" placeholder="Maximum price"/>
                            </div>
                        </div>
                        
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-success" name="isFilter" value="yes">Search</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<hr>
<div class="row">
@foreach($properties as $property)
<div class="card" style="width: 18rem;">
  <img src="https://images.squarespace-cdn.com/content/v1/5a0a8c668c56a8aa90ec1c42/1552855787969-99QHLGW0N8TYXMPT56RB/ke17ZwdGBToddI8pDm48kC2Ou1MO9kamcKms_rlnXOx7gQa3H78H3Y0txjaiv_0fDoOvxcdMmMKkDsyUqMSsMWxHk725yiiHCCLfrh8O1z4YTzHvnKhyp6Da-NYroOW3ZGjoBKy3azqku80C789l0uaXNx80D0McfB72Ma0lflo0nlext0JZ61zpqcjChfkagJk728J7DlqVoX33-EFZ3Q/Placeholder1.png" class="card-img-top" alt="...">
  <div class="card-body">
    <h5 class="card-title">{{$property->title}}</h5>
    <p class="card-text">Price: {{$property->price}}</p>
    <a href="{{route('home.show',base64_encode($property->propertyId))}}" class="btn btn-primary">Detail</a>
  </div>
</div>
@endforeach
</div>
@stop