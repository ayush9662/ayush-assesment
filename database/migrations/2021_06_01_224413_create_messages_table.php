<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('messages', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('messageId')->autoIncrement();
            $table->string('name')->nullable();
            $table->string('email')->nullable();
            $table->string('contact')->nullable();
            $table->longText('message')->nullable();
            $table->integer('sendUserId')->nullable();
            $table->integer('enquiredUser');
            $table->index('enquiredUser');
            $table->foreign('enquiredUser')->references('userId')->on('users');
            $table->integer('enquiredProperty');
            $table->index('enquiredProperty');
            $table->foreign('enquiredProperty')->references('propertyId')->on('properties');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('messages');
    }
}
