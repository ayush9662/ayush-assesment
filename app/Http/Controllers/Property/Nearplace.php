<?php

namespace App\Http\Controllers\Property;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Place;
use App\Property;

class Nearplace extends Controller
{
    //
    public function index($id)
    {
        $id=base64_decode($id);
        $data=Property::where('propertyId',$id)->first();
        if($data)
        {
            $places=Place::where('placePropertyId',$id)->get();
            return view('images.add-place',compact('data','places'));
        }
        return redirect()->route('property.index')->with('warning',"No record found");
        
    }

    //image upload
    public function addPlace(Request $request)
    {
        $request->validate([
            'place' => 'required',
        ]);

          $place = new Place;
          $place->placeLocation = $request->input('place');
          $place->placePropertyId = $request->input('propertyId');
          $place->save();
          return redirect()->back()->with('success','Nearby place has been added successfully');
  
          
    }

    //
    public function delete($id)
    {
        $id=base64_decode($id);
        Place::where('placeId',$id)->delete();
        return redirect()->back()->with('success','Nearby place has been deleted successfully');
    }
}
