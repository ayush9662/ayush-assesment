@extends('layout.layout')

@section('title','Property Detail')

@section('content')
<style>
* {
  box-sizing: border-box;
}

/* Style inputs */
input[type=text], select, textarea {
  width: 100%;
  padding: 12px;
  border: 1px solid #ccc;
  margin-top: 6px;
  margin-bottom: 16px;
  resize: vertical;
}

input[type=submit] {
  background-color: #04AA6D;
  color: white;
  padding: 12px 20px;
  border: none;
  cursor: pointer;
}

input[type=submit]:hover {
  background-color: #45a049;
}

/* Style the container/contact section */
.container {
  border-radius: 5px;
  background-color: #f2f2f2;
  padding: 10px;
}

/* Create two columns that float next to eachother */
.column {
  float: left;
  width: 50%;
  margin-top: 6px;
  padding: 20px;
}

/* Clear floats after the columns */
.row:after {
  content: "";
  display: table;
  clear: both;
}

/* Responsive layout - when the screen is less than 600px wide, make the two columns stack on top of each other instead of next to each other */
@media screen and (max-width: 600px) {
  .column, input[type=submit] {
    width: 100%;
    margin-top: 0;
  }
}

</style>
<div class="container">
  <div style="text-align:center">
    <h2>Property Details</h2>
    
  </div>
  <div class="row">
    <div class="column">
      <table  class="table">
      <tr><td><b>Property Title : </b></td><td>{{$data->title}}</td></tr>
      <tr><td><b>Floor Area(Sqft) : </b></td><td>{{$data->area}}</td></tr>
      <tr><td><b>Price : </b></td><td>{{$data->price}}</td></tr>
      <tr><td><b>Bedroom : </b></td><td>{{$data->bedroom}}</td></tr>
      <tr><td><b>Bathroom : </b></td><td>{{$data->bathroom}}</td></tr>
      <tr><td><b>Address : </b></td><td>{{$data->address}}</td></tr>
      <tr><td><b>City: </b></td><td>{{getCityName($data->city)}}</td></tr>
      <tr><td><b>Address : </b></td><td>{{$data->address}}</td></tr>
      <tr><td><b>Description : </b></td><td>{{$data->description}}</td></tr>
      </table>
    </div>
    <div class="column">
   
    </div>
  </div>
</div>
@stop