<?php

namespace App\Http\Controllers\Property;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\City;
use App\Services\Property;


class Basicinfo extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data=Property::getProperty(); 
        return view('agent.property-listing',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $data=City::all();
        return view('agent.add-property',compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $data=$request->all();
       $propertyAdded=Property::addProperty($data);
       if($propertyAdded)
       {
           return redirect()->route('property.index')->with('success','Property added successfully');
       }
       return redirect()->route('property.create')->with('warning','Property not added');
    

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $data=Property::getFrontDetails($id);
        if($data)
        {
            return view('agent.view-property',compact('data'));
        }
        return redirect()->back();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $data=Property::getPropertyForm($id);
        $city=City::all();
        if($data)
        {
            return view('agent.edit-property',compact('data','city'));
        }
        return redirect()->route('property.index')->with('warning','No record found');
       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $data=$request->all();
        $updated_property=Property::updatePropertyData($data,$id);
        if($updated_property)
        {
            return redirect()->route('property.index')->with('success','You have updated property successfully');

        }
        return redirect()->route('property.create')->with('warning','Property update failed');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $deleted=Property::softDeleteProperty($id);
        if($deleted)
        {
            return redirect()->route('property.index')->with('success','You have deleted property successfully');

        }
        return redirect()->route('property.create')->with('warning','Property not found');
        
    }
}
