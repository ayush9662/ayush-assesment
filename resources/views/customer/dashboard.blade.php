@extends('layout.layout')

@section('title','Customer Dashboard')

@section('content')
@if (Session::get('success'))
<div class="alert alert-success alert-dismissible">
    <button style="color:#fff" type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

    {{Session::get('success')}}
</div>
@elseif (Session::get('warning'))
<div class="alert alert-danger alert-dismissible">
    <button style="color:#fff" type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

    {{Session::get('warning')}}
</div>
@endif

@stop