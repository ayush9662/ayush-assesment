<?php

namespace App\Http\Controllers\Property;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Property;
use App\Galleryimage;

class GalleryImages extends Controller
{
    //
    public function index($id)
    {
        $id=base64_decode($id);
        $data=Property::where('propertyId',$id)->first();
        if($data)
        {
            $images=Galleryimage::where('galleryPropertyId',$id)->get();
            return view('images.add-gallery-images',compact('data','images'));
        }
        return redirect()->route('property.index')->with('warning',"No record found");
        
    }

    //image upload
    public function imageUpload(Request $request)
    {
        $request->validate([
            'gallery' => 'required|image|mimes:jpeg,png,jpg|max:3000|min:1',
        ],[
            "gallery.required"=>"Please select one gallery image",
            "gallery.image"=>"File should be an image",
            "gallery.mimes"=>"Files should be jpeg,png,jpg",
            "gallery.max"=>"File size should not be greater than 3 MB",
            "gallery.min"=>"Please select a valid file"
        ]);
  
         
  
          if ($request->file('gallery')) {
              $imagePath = $request->file('gallery');
              $imageName = $imagePath->getClientOriginalName();
  
              $path = $request->file('gallery')->storeAs('uploads', $imageName, 'public');
          }

          $image = new Galleryimage;
          $image->image = $imageName;
          $image->galleryPropertyId = $request->input('propertyId');
          $image->save();
  
          return response()->json('Image uploaded successfully');
    }

    //
    public function delete($id)
    {
        $id=base64_decode($id);
        Galleryimage::where('galleryImageId',$id)->delete();
        return redirect()->back()->with('success','Gallery image has been deleted successfully');
    }
}
