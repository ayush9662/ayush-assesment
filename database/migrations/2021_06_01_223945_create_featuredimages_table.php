<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFeaturedimagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('featuredimages', function (Blueprint $table) {
            $table->engine = 'InnoDB';
            $table->integer('featuredImageId')->autoIncrement();
            $table->integer('featurePropertyId');
            $table->index('featurePropertyId');
            $table->foreign('featurePropertyId')->references('propertyId')->on('properties');
            $table->longText('image')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('featuredimages');
    }
}
