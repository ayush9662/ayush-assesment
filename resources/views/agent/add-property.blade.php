@extends('layout.layout')

@section('title','Add Property')

@section('content')

    <b><br>Add your property here.
        <hr>
        @if (Session::get('success'))
        <div class="alert alert-success alert-dismissible">
            <button style="color:#fff" type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

            {{Session::get('success')}}
        </div>
        @elseif (Session::get('warning'))
        <div class="alert alert-danger alert-dismissible">
            <button style="color:#fff" type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

            {{Session::get('warning')}}
        </div>
        @endif

        <form action="{{route('property.store')}}" method='post' id="propertyForm">
            @csrf
         <div class="row">
           <div class="col-md-6">
            <div class="form-group">
                <label for="exampleInputEmail1">Property Title</label>
                <input type="text" name="title" id="title" class="form-control" placeholder="Enter property title" value="{{old('title')}}">
                <span style='color:red'>
                    @error('title'){{$message}} @enderror
                </span>
            </div>
            </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="exampleInputEmail1">Price</label>
                <input type="text" name="price" id="price" class="form-control" placeholder="Enter property price" value="{{old('price')}}">
                <span style='color:red'>
                    @error('price'){{$message}}@enderror
                </span>
            </div>
        </div>
        </div>
        <div class="row">
           <div class="col-md-6">
            <div class="form-group">
                <label for="exampleInputEmail1">Floor Area(In SQFT)</label>
                <input type="text" name="area" id="area" class="form-control" placeholder="Enter floor area" value="{{old('area')}}">
                <span style='color:red'>
                    @error('area'){{$message}} @enderror
                </span>
            </div>
            </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="exampleInputEmail1">No of Bedrooms</label>
                <input type="number" name="bedroom" id="bedroom" class="form-control" placeholder="Enter no of bedrooms" min="1" step="1" value="{{old('bedroom')}}">
                <span style='color:red'>
                    @error('bedroom'){{$message}}@enderror
                </span>
            </div>
        </div>
        </div>
        <div class="row">
           <div class="col-md-6">
           <div class="form-group">
                <label for="exampleInputEmail1">No of Bathrooms</label>
                <input type="number" name="bathroom" id="bathroom" class="form-control" placeholder="Enter no of bedrooms" min="1" step="1" value="{{old('bathroom')}}">
                <span style='color:red'>
                    @error('bathroom'){{$message}}@enderror
                </span>
            </div>
            
            </div>
        <div class="col-md-6">
        <div class="form-group">
                <label for="exampleInputEmail1">City</label>
                <select name="city" id="city" class="form-control">
                <option value="">--Select City--</option>
                @foreach($data as $city)
                <option value="{{$city->cityId}}">{{$city->cityName}}</option>
                @endforeach
                </select>
                  <span style='color:red'>
                    @error('city'){{$message}} @enderror
                </span>
            </div>
        </div>
        </div>
        <div class="row">
           <div class="col-md-6">
            <div class="form-group">
                <label for="exampleInputEmail1">Address</label>
                <input type="text" name="address" id="address" class="form-control" placeholder="Enter property address" value="{{old('address')}}">
                <span style='color:red'>
                    @error('address'){{$message}} @enderror
                </span>
            </div>
            </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="exampleInputEmail1">Description</label>
                <textarea class='form-control' name="description" id="description">{{old('description')}}</textarea>
                <span style='color:red'>
                    @error('description'){{$message}}@enderror
                </span>
            </div>
        </div>
        </div>
            <button type="submit" class="btn btn-success" name='addProperty' value='yes'>Add Property</button>
        </form>
       

@stop