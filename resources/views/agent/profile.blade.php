@extends('layout.layout')

@section('title','Agent Profile')

@section('content')
<div class='col-sm-6'>
    <b><br>Your profile
        <hr>
        @if (Session::get('success'))
        <div class="alert alert-success alert-dismissible">
            <button style="color:#fff" type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

            {{Session::get('success')}}
        </div>
        @elseif (Session::get('warning'))
        <div class="alert alert-danger alert-dismissible">
            <button style="color:#fff" type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

            {{Session::get('warning')}}
        </div>
        @endif


        <form action="{{route('agent.update',base64_encode($data->userId))}}" method='post' id="registerForm" enctype="multipart/form-data">
        @method('PUT')
            @csrf
            <img class="profile_img" src='{{asset("storage/uploads/$data->userImage")}}' alt="" />
            <div class="form-group">
                <label for="exampleInputEmail1">Email</label>
                <input type="email" name="email" id="email" class="form-control" placeholder="Enter your email" value="{{$data->userEmail}}" readonly>
                <span style='color:red'>
                    @error('email'){{$message}} @enderror
                </span>
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Name</label>
                <input type="name" name="name" id="name" class="form-control" placeholder="Enter your email" value="{{$data->userName}}">
                <span style='color:red'>
                    @error('email'){{$message}} @enderror
                </span>
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Password</label>
                <input type="password" name="password" id="password" class="form-control" placeholder="Enter your password" value="{{base64_decode($data->password)}}">
                <span style='color:red'>
                    @error('password'){{$message}}@enderror
                </span>
            </div>
            <div class="form-group">
                <label for="exampleInputEmail1">Profile Image</label>
                <input type="file" name="file" id="file" class="form-control" accept="image/x-png,image/jpg,image/jpeg">
                <span style='color:red'>
                    @error('file'){{$message}}@enderror
                </span>
            </div>
            <button type="submit" class="btn btn-success" name='isUpdate' value='yes'>Update</button>
        </form>
       
</div>


@stop