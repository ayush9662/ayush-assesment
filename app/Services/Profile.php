<?php

namespace App\Services;

use App\Repositories\Profilerepo;
use Illuminate\Support\Facades\Validator;

class Profile
{

    public static function getProfileDetails($id)
    {
        $id=base64_decode($id);
        return Profilerepo::profileData($id);
    }

    public static function updateUserData($data,$id)
    {
        $id=base64_decode($id);
        if(isset($data['isUpdate']))
        {
            Validator::make($data,[
                "name"=>'required',
                "password"=>"required|min:6"
            ])->validate();
         
    
            if(isset($data['file']))
            { 
                Validator::make($data,[
                    'file'=>'image|mimes:png,jpeg,jpg|max:3000'
                ])->validate();
                $file=$data['file'];
                $imageName=$file->getClientOriginalName();
                $file->storeAs('uploads',$imageName,'public');
               
            }
          return Profilerepo::updateProfile($data,$id);
        }

    }
}