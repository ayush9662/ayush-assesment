@extends('layout.layout')

@section('title','Agent Dashboard')

@section('content')
@if (Session::get('success'))
<div class="alert alert-success alert-dismissible">
    <button style="color:#fff" type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

    {{Session::get('success')}}
</div>
@elseif (Session::get('warning'))
<div class="alert alert-danger alert-dismissible">
    <button style="color:#fff" type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>

    {{Session::get('warning')}}
</div>
@endif
<br>
<h4>Enquaries:</h4>
<table class="table">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col"> Name</th>
      <th scope="col">Email</th>
      <th scope="col">Contact</th>
      <th scope="col">Action</th>
    </tr>
  </thead>
  <tbody>@php $n=1; @endphp
  @foreach($data as $message)
    <tr>
      <th scope="row">{{$n++}}</th>
      <td>{{$message->name}}</td>
      <td>{{$message->email}}</td>
      <td>{{$message->contact}}</td>
      
      <td> 
        <a href="{{route('agent.show',base64_encode($message->messageId))}}"><i class="fa fa-eye"></i></a>
       
      </td>
    </tr>
    @endforeach
    
  </tbody>
</table>
{{$data->links()}}
@stop